from typing import List, Optional
import typing
import strawberry

@strawberry.type
class Patient:
    ssn: str
    first_name: str
    last_name: str
    maiden_name: str
    birth_date: str
    gender: str
    address: 'Address'
    insurance: 'Insurance'

    
@strawberry.type
class Address:
    street1: str
    street2: str
    city: str
    state: str
    zipcode: str


@strawberry.type
class Insurance:
    name: str
    plan: str
    group: str
    member: str


patients = [
    Patient(
        ssn='123-45-6789',
        first_name='Chris', 
        last_name='Smith', 
        maiden_name='', 
        birth_date='1981-12-02', 
        gender='male', 
        address=Address(
            street1='1313 Mockingbird Lane',
            street2='',
            city='Anytown',
            state='TX',
            zipcode='12345'
        ),
        insurance=Insurance(
            name='Aetna', 
            plan='GA HMO Gold', 
            group='FD9928', 
            member='2343244'
        )
    ),
        Patient(
        ssn='333-44-3321',
        first_name='Hua', 
        last_name='Jansky', 
        maiden_name='Persons', 
        birth_date='1956-11-22', 
        gender='female', 
        address=Address(
            street1='156 Main St',
            street2='Unit 3',
            city='Burkwood',
            state='AK',
            zipcode='99499'
        ),
        insurance=Insurance(
            name='Blue Cross', 
            plan='Plan 1', 
            group='8908BBHD', 
            member='23309'
        )
    )
]


@strawberry.type
class Visit:
    entrance: str
    checkin: str
    admission: str
    discharge: str
    patient_id: str

visits = [
    Visit(entrance='2022-04-22T13:11', checkin='2022-04-22T14:01', admission='', discharge='', patient_id='123-45-6789'),
    Visit(entrance='2019-01-12T03:54', checkin='2019-01-12T06:04', admission='2019-01-12T09:15', discharge='2019-01-15T11:33', patient_id='123-45-6789')
]



@strawberry.type
class Query:
    @strawberry.field
    def visits(self, id: strawberry.ID) -> List[Visit]:
        return visits

    @strawberry.field
    def patients(self, id: Optional[str] = None) -> List[Patient]:
        if id:
            print(f'Searching for patient ID: {id}')
            # Should add call to FHIR API here to return a patient
            results = [p for p in patients if p.ssn == id]
        else: 
            print('Returning all patients')
            results = patients
        return results


@strawberry.type
class Mutation:
    @strawberry.mutation
    def add_visit(self, entrance: str, checkin: str, admission: str, discharge: str, patient_id: str) -> Visit:
        print(f'Adding visit {entrance} entrance by {patient_id}')
        v = Visit(entrance=entrance, checkin=checkin, admission=admission, discharge=discharge, patient_id=patient_id)
        visits.append(v)
        return v

schema = strawberry.Schema(query=Query, mutation=Mutation)


