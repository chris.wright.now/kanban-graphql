# CS6440 Spring 2022
## Chris Wright gtg123s@gatech.edu
This is an API that provides very basic Patient, Insurance, Address, and Visit log resources for use with the Kanban Patient Flow concept.

## How to run
1. Create a Python virtual environment, e.g., `python -m venv graphqlenv`
2. Activate the virtual environment
3. Install the requirements `pip install -r requirements.txt`
4. Run the API server process `strawberry server schema`
5. Visit http://localhost:8000

You can now interact with the API using the graphical interface. 

## Example queries
### Query: All Patients
1. Delete any content in the query editor
2. Paste the following:
```
query MyPatients {
  patients(id: "") {
    birthDate
    gender
    address {
      city
      state
      street1
      street2
      zipcode
    }
    firstName
    insurance {
      group
      member
      name
      plan
    }
    lastName
    maidenName
    ssn
  }
}
```
3. Click the Run button


### Query: Search Patients by Patient ID
1. Delete any content in the query editor
2. Paste the following:
```
query SearchPatients {
  patients(id: "123-45-6789") {
    birthDate
    gender
  }
}
```
3. Click the Run button

### Query: Show all Visits
1. Delete any content in the query editor
2. Paste the following:
```
query MyVisits {
  visits(id: "") {
    admission
    checkin
    discharge
    entrance
    patientId
  }
}
```
3. Click the Run button

### Mutation: Add a Visits
1. Delete any content in the query editor
2. Paste the following:
```
mutation AddUpdateVisit {
  addVisit(admission: "2022-04-11T05:11", checkin: "", discharge: "", entrance: "", patientId: "123-45-6789") {
    admission
    checkin
    discharge
    entrance
    patientId
  }
}
```
3. Click the Run button

